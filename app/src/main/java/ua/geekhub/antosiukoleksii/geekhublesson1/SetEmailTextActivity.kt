package ua.geekhub.antosiukoleksii.geekhublesson1

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_set_email_text.*

class SetEmailTextActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_email_text)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        if (savedInstanceState.containsKey("inputSubject")) edtSubject.setText(savedInstanceState.getString("inputSubject"))
        if (savedInstanceState.containsKey("inputBody")) edtEmailBody.setText(savedInstanceState.getString("inputBody"))
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putString("inputSubject", edtSubject.text.toString())
        outState.putString("inputBody", edtEmailBody.text.toString())
    }

    fun onClickApply(view: View) {
        val intent = Intent()
        intent.putExtra("subject", edtSubject.text.toString())
        intent.putExtra("body", edtEmailBody.text.toString())
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    fun onClickCancel(view: View) {
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

}
