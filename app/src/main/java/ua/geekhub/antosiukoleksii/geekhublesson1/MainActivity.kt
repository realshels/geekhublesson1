package ua.geekhub.antosiukoleksii.geekhublesson1

import android.app.Activity
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val SET_EMAIl_TEXT = 100;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        if (savedInstanceState.containsKey("subject")) txtSubjectBody.text = savedInstanceState.getString("subject")
        if (savedInstanceState.containsKey("body")) txtMailBody.text = savedInstanceState.getString("body")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putString("subject", txtSubjectBody.text.toString())
        outState.putString("body", txtMailBody.text.toString())
    }

    fun onClickSetNewEmail(view: View) {
        val intent = Intent(this, SetEmailTextActivity::class.java)
        startActivityForResult(intent, SET_EMAIl_TEXT)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == SET_EMAIl_TEXT && resultCode == Activity.RESULT_OK) {
            if (data != null) {
                if (data.hasExtra("subject")) txtSubjectBody.setText(data.getStringExtra("subject"))

                if (data.hasExtra("body")) txtMailBody.text = data.getStringExtra("body")
            }
        }
    }

    fun onClickEmailSend (view: View) {
        val intent = Intent(Intent.ACTION_SENDTO, Uri.parse("mailto:"))
        intent.putExtra(Intent.EXTRA_SUBJECT, txtSubjectBody.text.toString())
        intent.putExtra(Intent.EXTRA_TEXT, txtMailBody.text.toString())
        startActivity(Intent.createChooser(intent, "Choose email app"))
    }
}
